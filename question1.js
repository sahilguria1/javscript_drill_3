//function to find user intreted in video game
function getVideoGamePlayer(data, interest) {
  //handle edge cases.
  if (data == undefined || data == null) {
    return "Provide correct data.";
  } else if (data.length == 0) {
    return "Data is empty.";
  } else if (interest == undefined || interest == null) {
    return "Provide correct interest.";
  }

  let userPlayVideoGames = []; // creating list for output list
  const indexOfdata = data.length; //length of data

  //finding the user from loop
  for (const index in data) {
    let userInterest = String(data[index].interests);
    if (userInterest.includes(interest)) {
      userPlayVideoGames.push(index);
    }
  }
  return userPlayVideoGames;
}
module.exports = getVideoGamePlayer;
