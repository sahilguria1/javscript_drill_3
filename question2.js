// function that give user by country
function getUserByCountry(data, country) {
  //handle edge cases.
  if (data == undefined || data == null) {
    return "Provide correct data.";
  } else if (data.length == 0) {
    return "Data is empty.";
  } else if (country == undefined || country == null) {
    return "Provide some Country.";
  }

  let userByCountry = []; // creating list for output list
  const indexOfdata = data.length; //length of data

  //finding the user from loop
  for (const index in data) {
    let userInterest = String(data[index].nationality);
    if (userInterest == country) {
      userByCountry.push(index);
    }
  }
  return userByCountry;
}
module.exports = getUserByCountry;