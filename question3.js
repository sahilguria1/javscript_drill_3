//function to get user by degree
function getUserByDegree(data, degree) {
  //handle edge cases.
  if (data == undefined || data == null) {
    return "Provide correct data.";
  } else if (data.length == 0) {
    return "Data is empty.";
  } else if (degree == undefined || degree == null) {
    return "Provide some degree.";
  }

  let userByDegree = []; // creating list for output list
  const indexOfdata = data.length; //length of data

  //finding the user from loop
  for (const index in data) {
    let userQualification = String(data[index].qualification);
    if (userQualification == degree) {
      userByDegree.push(index);
    }
  }
  return userByDegree;
}
module.exports = getUserByDegree;
