//get user by Based On Programming lang
function getUserBasedOnProgramming(data) {
  //handle edge cases.
  if (data == undefined || data == null) {
    return "Provide correct data.";
  } else if (data.length == 0) {
    return "Data is empty.";
  }

  let groupedUsers = {};
  //Iterate over each user in the users object
  for (const [name, details] of Object.entries(data)) {
    // Extract the programming language from the designation
    const language = details.desgination.split(" ")[1];

    // Initialize the array for the language if it doesn't exist
    if (!groupedUsers[language]) {
      groupedUsers[language] = [];
    }

    // Add the user to the appropriate language group
    groupedUsers[language].push({ name, ...details });
  }

  return groupedUsers;
}
module.exports = getUserBasedOnProgramming;
